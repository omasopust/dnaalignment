<?php
/**
 * User: Petr, 11:32 - 19.10.13
 * To change this template use File | Settings | File Templates.
 */

if ($argc != 3) {
    echo "Zadejte dva parametry - dva retezce pro porovnani.\n";
    exit;
}

require_once("./../app/components/LocalAlign.php");
require_once("./../app/components/ScoreMatrix.php");
require_once("./../app/components/Matrix.php");
require_once("./../app/components/Position.php");
require_once("./../app/components/LocalAlignResult.php");

$localAlign = new LocalAlign();
$result = $localAlign->compare($argv[1], $argv[2]);

echo "Zarovnani:\n\n";
echo "DNA1 (".strlen($result->getDna1())."): ".$result->getDna1()."\n";
echo "DNA2 (".strlen($result->getDna2())."): ".$result->getDna2()."\n\n";
echo "Skore: ". $result->getScore()."\n";
if ($result->getScore() == 0) {
    echo "Retezce DNA si nejsou vubec podobne.";
}
else {
    echo $result->getPlain()."\n";
}

echo "\nOK\n";
exit;