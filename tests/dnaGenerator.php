<?php

echo "Generator nahodnych retezcu DNA:\n";

$chars = array(0 => "A", 1 => "C", 2 => "T", 3 => "G");

for($i = 0; $i < 100; $i++) {
    $dna = "";
    for($j = 0; $j < 10; $j++) {
        $dna .= $chars[mt_rand(0,1000) % 4];
    }
    echo $dna."\n";
}
