/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : dnaalignment

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2013-12-08 23:28:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sequence`
-- ----------------------------
DROP TABLE IF EXISTS `sequence`;
CREATE TABLE `sequence` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `string` text,
  `section` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `photo` varchar(100) DEFAULT 'default.gif',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sequence
-- ----------------------------
INSERT INTO `sequence` VALUES ('1', 'ATGGTTAAGGTTTATGCCCCGGCTTCCAGTGCCAA', '2800-3732', 'Escherichia coli', 'ecoli.jpg');
INSERT INTO `sequence` VALUES ('2', 'ATGACAGATACCGTGTTTCCCGCACATCTTCTGAAG', '131486-132067', 'Acetobacter pasteurianus', 'acetobacter.jpg');
INSERT INTO `sequence` VALUES ('3', 'TAGCAGTTGTAATTTCAGCAGTATTGTCGGATCTTGTAA', '7766-7838', 'Anopheles gambiae', 'anopheles.jpg');
INSERT INTO `sequence` VALUES ('8', 'TGTGGATAACTTTATCCCCCAGGAGAGACGCTTTTATGTTACTATTTTAATAC', '549142279', 'Acholeplasma brassicae', 'acholeplasma.png');
INSERT INTO `sequence` VALUES ('9', 'TAAACTGAAGAGTTTGATCCTGGCTCAGATTGAGCGCTAACGGGAAGCCTT', '3924211-3925743', 'Achromobacter xylosoxidans', 'achromobacter.jpg');
INSERT INTO `sequence` VALUES ('10', 'CAAGTGAATAAGCGTACACGGTGGATGCCTTGGCAGCCAGAGGCGATGAAGGA', '2257877-2260781', 'Alcanivorax borkumensis', 'alcanivorax.jpg');
INSERT INTO `sequence` VALUES ('11', 'AGGGGCGTAGCTCAACTGGCAGAGCAGCGGTCTCCAAAACCGCAGGTTGCAGGT', '404283-404355', 'Amycolicicoccus subflavus', 'amycolicicoccus.jpg');
INSERT INTO `sequence` VALUES ('12', 'GCCCGGATAGCTCAGTCGGTAGAGCAGGGGACTGAAAATCCCCGTGTCGGTGGT', '17956-18028', 'Buchnera aphidicola', 'buchnera.jpg');
INSERT INTO `sequence` VALUES ('13', 'TGGGGGTGTCGCATAGCGGTCAATTGCATCGGACTGTAAATCCGACTCCTTACG', '159382-159466', 'Chlamydia trachomatis', 'chlamydia.jpg');
INSERT INTO `sequence` VALUES ('14', 'GCGGATGTGGCGGAATGGCAGACGCGCAAGATTCAGGATCTTGTGGGGTAAAA', '460848-460934', 'Dictyoglomus turgidum', 'dictyoglomus.jpg');
INSERT INTO `sequence` VALUES ('15', 'TCCTCCTTAGCTCAGTTGGTTAGAGCATCTGACTGTTAATCAGAGGGTCCTTGGTTC', '113421-113494', 'Flavobacterium indicum', 'flavobacterium.jpg');
INSERT INTO `sequence` VALUES ('16', 'GGGGCCTTAGCTCAGCTGGGAGAGCGCCTGCTTTGCAAGCAGGAGGTCATCGGT', '12525-12600', 'Geobacillus', 'geobacillus.jpg');
INSERT INTO `sequence` VALUES ('17', 'GGCTCCTTCATCTAGTGGTTAGGATACCACCCTTTCACGGTGGTTACAGGGGTT', '4619-4693', 'Helicobacter pylori', 'helicobacter.jpg');
INSERT INTO `sequence` VALUES ('18', 'GGGCAGTTAGCTCAGTTGGTAGAGCAACGGACTCTTAATCCGTGGGTCCAGGG', '22640-22715', 'Lactobacillus buchneri', 'lactobacillus.jpg');
INSERT INTO `sequence` VALUES ('19', 'GGTCTGGTAGTTCAGTTGGTTAGAATGCCTGCCTGTCACGCAGGAGGTCGCGAGT', '236378-236451', 'Marivirga tractuosa', 'marivirga.png');
INSERT INTO `sequence` VALUES ('21', 'GCGGGTGTAGCTCAGGGGTAGAGCACGACCTTGCCAAGGTCGGGGTCGAGGGTT', '95235-95309', 'Nitrobacter hamburgensis', 'nitrobacter.jpg');
INSERT INTO `sequence` VALUES ('22', 'AGAGTTTGATCATGGCTCAGATTGAACGCTGGCGGCAGGCCTAACACATGCA', '50959-52493', 'Spiroplasma diminutum', 'spiroplasma.jpg');
INSERT INTO `sequence` VALUES ('23', 'GGAGGGTTCGCATAGCGGCCGAGTGCGACGCTCTTGAAAAGCGTTAGGTCGGTA', '106706-106795', 'Thermobifida fusca', 'thermobifida.jpg');
INSERT INTO `sequence` VALUES ('24', 'GGGAAGATGGCAGAGTGGTTGAATGTACCTGACTCGAAATCAGGCAGGCGTTTATA', '24508-24598', 'Xylella fastidiosa', 'xylella.jpg');
INSERT INTO `sequence` VALUES ('25', 'GGCTCGATGGCGGAGTGGTTACGCAGAGGACTGCAAATCCTTGCACGCCGGTTC', '113543-113616', 'Zymomonas mobili', 'zymomonas.jpg');
