<?php

use Nette\Application\UI\Form,
    \Forms\Dna\DnaForm;

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter {

    const MAX_RESULTS = 10;

    public function actionDefault() { }

    public function actionCompare() { }

    // --- VYHLEDAVANI ------------------------------------------------------

    public function createComponentSearchForm() {
        $form = new DnaForm();

        $form->addSubmit('submit', 'Vyhledat')
            ->setAttribute('class', 'btn btn-primary');

        $form->onValidate[] = callback($this, 'validateSearchForm');
        $form->onSuccess[] = callback($this, 'search');

        return $form;
    }

    public function validateSearchForm(Form $form) {
        $this->validateDna($form, "dna");
    }

    public function search(Form $form) {
        $values = $form->getValues(true);
        $this->template->results = null;

        Timer::start("query");  // stopovani doby zpracovani jednoho dotazu
        $AllSequences = $this->context->dnaRepository->findAll();
        // overeni zda v databazi vubec neco je
        if (count($AllSequences) < 1) {  // v databazi nemame ulozene zadne retezce dna
            //$form->addError("Databáze řetězců DNA je prázdná.");
            return;
        }

        try {
            // inicializace tridy provadejici zarovnani
            if ($values['algorithm'] == DnaForm::GLOBAL_ALIGN) {    // TODO upravit inicializaci tridy globalniho zarovnani
                $align = new GlobalAlign($values["matchScore"], $values["mismatchScore"], $values["gapPenalty"]);
            }
            else {
                $align = new \LocalAlign($values["matchScore"], $values["mismatchScore"], $values["gapPenalty"]);
            }
             
            // zarovnani hledaneho retezce se vsemi retezci v databazi
            foreach($AllSequences as $sequence) {
                Timer::start("align_".$sequence->id);   // zmereni doby zarovani vsech dvojic
                
                $this->template->results[] = array(
                    "sequence" => $sequence,
                    "result" => $align->compare($values["dna"], $sequence->string)
                );
                
                Timer::stop("align_".$sequence->id);
            }
        } catch (\Exception $e) {
            $form->addError($e->getMessage());
            $this->template->results = null;
        }

        // serazeni vsech zarovnani podle skore od nejvyssiho
        usort($this->template->results, callback($this, "compareScoresInEntryArray"));
        // oriznuti pole na max počet prvků
        $this->template->results = array_slice($this->template->results, 0, self::MAX_RESULTS);
        // ukonceni mereni casu zpracovani celeho dotazu
        Timer::stop("query");
        $this->logTime("Dotaz delky: ".strlen($values["dna"])." znaku - doba zpracovani: ".Timer::printTime("query") . ".");
    }

    // --- POROVNAVANI - PRO TESTOVANI -------------------------------

    public function createComponentCompareForm() {
        $form = new DnaForm();

        $form->addTextArea('dna2', "Řetězec DNA:", 40, 2)
//          ->setValue($form::generateRandomDNA())
            ->addCondition(Form::FILLED)
                ->addRule(Form::PATTERN, "Řetězec smí obsahovat pouze znaky A, C, T, G.", '^[actgACTG\s]+$');

        $form->addUpload('dna2_file','Soubor s DNA')
            //->addRule(Form::MIME_TYPE, 'Zadejte textový soubor.', "text/*")
            ->addRule(Form::MAX_FILE_SIZE, 'Zadejte soubor o maximální velikosti 2MB.', 2*1024*1024);
        $form->addSubmit('submit', 'Porovnat')
            ->setAttribute('class', 'btn');

        $form->onValidate[] = callback($this, 'validateCompareForm');
        $form->onSuccess[] = callback($this, 'compare');

        return $form;
    }

    public function validateCompareForm(Form $form) {
        $this->validateDna($form, "dna");
        $this->validateDna($form, "dna2");

    }

    public function compare(Form $form) {
        try {
            $values = $form->getValues(true);
            $this->template->result = null;

            // zmereani doby zarovnani dvou retezcu
            Timer::start("compare");

            // inicializace algoritmu zarovnani
            if ($values['algorithm'] == DnaForm::GLOBAL_ALIGN) {        // TODO upravit inicializaci tridy globalniho zarovnani
                $align = new GlobalAlign();
            }
            else {
                    $align = new \LocalAlign($values["matchScore"], $values["mismatchScore"], $values["gapPenalty"]);
            }
            // zarovnani retezcu DNA
            $this->template->result = $align->compare($values["dna"], $values["dna2"]);

            Timer::stop("compare");
            $this->logTime("Porovnani retezcu delky: ".strlen($values["dna"])." a ".strlen($values["dna2"])." znaku - doba zpracovani: ".Timer::printTime("compare") . ".", self::COMPARE_LOG);
        } catch (\Exception $e) {
            $form->addError($e->getMessage());
            $this->template->result = null;
        }
    }

    // ------ POMOCNE METODY --------------------------------------------

    /**
     * Kontrola zda uzivatel zadal platny DNA retezec a to bud primo nebo souborem.
     * @param Nette\Application\UI\Form $form odeslany formular
     * @param $fieldName nazav formularoveho pole, ve kterem je kontrolovana dna ulozena
     */
    protected function validateDna(Form $form, $fieldName) {
        $values = $form->getValues(true);
        $values[$fieldName] = $this->removeWhitespaces($values[$fieldName]);
        if (empty($values[$fieldName])) {   // musime zadat DNA bud primo nebo nahranim souboru
            $fileContent = $values[$fieldName."_file"]->getContents();
            if (empty($fileContent)) { // dna nebyla vubec zadana
                $form[$fieldName]->addError("Zadejte řetězec DNA.");
                return;
            }
            if (!preg_match('/^[actgACTG\s]+$/',$fileContent)) {   // kontrola obssahu souboru
                $form[$fieldName."_file"]->addError($values[$fieldName."_file"]->getName().": Řetězec smí obsahovat pouze znaky A, C, T, G.");
            }
            $values[$fieldName] = $this->removeWhitespaces($fileContent);
        }

        $form->saveValues($values);    // ulozeni hodnot z formulare pro dalsi pouziti a validaci
    }

    /**
     * Odstraneni bilych znaku z retezce.
     * @param string $dna
     * @return string
     */
    protected function removeWhitespaces($dna) {
        return preg_replace('/[ \t\n\r]/','',trim($dna));
    }

    /**
     * Porovnani dvou zarovnani podle skore. (sestupne razeni)
     * @param LocalAlignResult|GlobalAlignResult $a
     * @param LocalAlignResult|GlobalAlignResult $b
     * @return bool
     * @deprecated
     */
    public function compareScores($a, $b) {
        return $a->getScore() < $b->getScore();
    }
    
    /**
     * Porovnani dvou zarovnani podle skore. (sestupne razeni)
     * @param LocalAlignResult|GlobalAlignResult $a
     * @param LocalAlignResult|GlobalAlignResult $b
     * @return bool
     */
    public function compareScoresInEntryArray($a, $b) {
        return $a["result"]->getScore() < $b["result"]->getScore();
    }

}
