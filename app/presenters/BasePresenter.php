<?php

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    const SEARCH_LOG = "search.log";
    const COMPARE_LOG = "compare.log";

    /**
     * Vlastni log slouzici pro doby zpracovani dotazovani se a porovnavani retezcu DNA.
     * @param $message
     * @param string $logName nazev logu, do ktereho ma byt zprava zapsana.
     */
    protected function logTime($message, $logName = self::SEARCH_LOG) {
        file_put_contents(__DIR__."/../../log/".$logName, date("d.n.Y H:i:s - ").$message."\r\n", FILE_APPEND);
    }
}
