<?php

use \Nette\Application\UI\Form,
    Forms\Dna\DnaForm;
/**
 * User: Petr, 10:49 - 27.11.13
 * To change this template use File | Settings | File Templates.
 */
class ExperimentalPresenter extends BasePresenter {

    public function renderAll() {
        $repository = $this->context->dnaRepository;
        $this->template->all = $repository->findAll();
    }

    public function renderRandom() {
        $this->template->random = \Forms\Dna\DnaForm::generateRandomDNA();
    }

    public function renderTruncateDb() {
        $this->context->dnaRepository->deleteAll();
        $this->flashMessage("Obsah databáze byl vymazán.");
        $this->logTime("Vyprazdneni obsahu databaze.");
        $this->redirect("all");
    }

    public function createComponentInsertDNAForm() {
        $form = new Form();
        $form->addText("count", "Počet řetězců: ", 6)
            ->setDefaultValue(1);
        $form->addText("lengthFrom", "Min: ",6)
            ->setDefaultValue(10);
        $form->addText("lengthTo", "Max: ", 6)
            ->setDefaultValue(10);
        $form->addSubmit("submit", "Vložit");
        $form->onSuccess[] = callback($this, "insertRandom");
        return $form;
    }

    public function insertRandom(Form $form) {
        $values = $form->getValues(true);
        $repository = $this->context->dnaRepository;
        $randomStrings = array();
        for ($i = 0; $i < $values["count"]; $i++) {
            $length = mt_rand($values["lengthFrom"], $values["lengthTo"]);
            $randomStrings[] = DnaForm::generateRandomDNA($length);
        }
        $repository->insertAll($randomStrings);
        $this->logTime("Vlozeno ".$values["count"]." retezecu delky ".$values["lengthFrom"]."-".$values["lengthTo"].".");
        $this->flashMessage("Vloženo ".$values["count"]." řetězeců délky ".$values["lengthFrom"]."-".$values["lengthTo"].".");
        $this->redirect("all");
    }

}
