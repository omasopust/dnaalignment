<?php

namespace Forms\Dna;

use \Nette\Application\UI\Form,
    Nette\ComponentModel\IContainer;
/**
 * User: Petr, 21:07 - 18.11.13
 * To change this template use File | Settings | File Templates.
 */
class DnaForm extends Form {

    const GLOBAL_ALIGN = 1;
    const LOCAL_ALIGN = 2;

    
    private $defaultValues = array(
        'matchScore' => 5,
        'mismatchScore' => -3,
        'gapPenalty' => -4
    );
    /**
     * Obsah odeslaneho formulare, pristup metodou getValues()
     * @var array|\Nette\ArrayHash $savedValues
     */
    protected $savedValues = null;

    public function __construct(IContainer $parent = NULL, $name = NULL) {
        parent::__construct($parent, $name);

        $prompt = \Nette\Utils\Html::el('option')->setText("Zvolte algoritmus zarovnání")->class('prompt');
        $this->addSelect('algorithm', "Algoritmus zarovnání",array(
            self::GLOBAL_ALIGN => "Globální zarovnání - Needleman-Wunsch",
            self::LOCAL_ALIGN => "Lokální zarovnání - Smith-Waterman"
        ))
        ->setPrompt($prompt)
        // ->setValue(self::LOCAL_ALIGN)
        ->addRule(Form::IS_IN, "Zadejte algoritmus zarovnání.", array(self::GLOBAL_ALIGN, self::LOCAL_ALIGN));
        $this->addText('matchScore', "Skóre za shodu")
            //->setValue(5)
            ->addRule(Form::INTEGER, "Skóre musí být číslo.");

        $this->addText('mismatchScore', "Skóre za neshodu")
            //->setValue(-3)
            ->addRule(Form::INTEGER, "Skóre musí být číslo.");
        $this->addText('gapPenalty', "Penalizace za mezeru")
            //->setValue(-4)
            ->addRule(Form::INTEGER, "Penalizace za mezeru   musí být číslo.");

        $this->addTextArea('dna', "Řetězec DNA", 40, 2)
            //->setValue(self::generateRandomDNA())
            ->addCondition(Form::FILLED)
                ->addRule(Form::PATTERN, "Řetězec smí obsahovat pouze znaky A, C, T, G.", '^[actgACTG\s]+$');
        $this->addUpload('dna_file','Soubor s DNA')
            //->addRule(Form::MIME_TYPE, 'Zadejte textový soubor.', "text/*")
            ->addRule(Form::MAX_FILE_SIZE, 'Zadejte soubor o maximální velikosti 2MB.', 2*1024*1024);
        
        $this->setDefaults($this->defaultValues);
    }

    /**
     * Ziskani obsahu odeslaneho formulare. Pri prvnim volani teto metody se nactena data ulozi do protected promenne
     * formulare, aby bylo mozne tato data editovat
     * @param bool $asArray
     * @return array|\Nette\ArrayHash|null
     */
    public function getValues($asArray = false) {
        if (!$this->savedValues) {
            $this->savedValues = parent::getValues($asArray);
        }
        return $this->savedValues;
    }

    /**
     * Ulozeni obsahu formulare (napr: po uprave pri validaci)
     * @param array|\Nette\ArrayHash $values pozmeneny obsah formulare
     */
    public function saveValues($values) {
        $this->savedValues = $values;
    }

    /**
     * Vygenervani nahodneho DNA retezce.
     * @param int $length delka retezce, ktery ma byt vygenerovan
     * @return string
     */
    public static function generateRandomDNA($length = 10) {
        $chars = array(0 => "A", 1 => "C", 2 => "T", 3 => "G");
        $dna = "";
        for($i = 0; $i < $length; $i++) {
            $dna .= $chars[mt_rand(0,1000) % 4];
        }
        return $dna;
    }

}
