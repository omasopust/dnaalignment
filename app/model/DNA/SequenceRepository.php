<?php

namespace Dna;
/**
 * User: Petr, 16:46 - 9.11.13
 * To change this template use File | Settings | File Templates.
 */
class SequenceRepository extends \Dna\Repository {

    /**
     * Ulozeni jednoho radku do tabulky `dna`.
     * @param string $string retezec DNA
     */
    public function insert($string) {
        $this->getTable()->insert(array(
            "string" => $string
        ));
    }

    /**
     * Ulozeni vice retezcu DNA najednou do databaze.
     * @param array $strings pole retezcu DNA
     */
    public function insertAll($strings) {
        $rows = array();
        foreach($strings as $s) {
            $rows[] = array(
                "string" => $s
            );
        }
        $this->getTable()->insert($rows);
    }
}
