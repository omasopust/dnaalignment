<?php
/**
 * User: Petr, 18:23 - 2.11.13
 * Vysledek zarovani dvou retezcu DNA.
 */
class LocalAlignResult {

    /** @var int $score */
    protected $score;

    /** @var string - puvodni zadane retezce DNA */
    protected $dna1;
    protected $dna2;

    /** @var string - retezce DNA upravene pro vypis a zobrazeni zarovnani */
    protected $dna1Align;
    protected $dna2Align;


    /**
     * @param string $dna1
     * @param string $dna2
     * @param int $score
     * @param string $dna1Align
     * @param string $dna2Align
     */
    function __construct($dna1, $dna2, $score, $dna1Align, $dna2Align) {
        $this->dna1 = $dna1;
        $this->dna1Align = $dna1Align;
        $this->dna2 = $dna2;
        $this->dna2Align = $dna2Align;
        $this->score = $score;
    }

    /**
     * @return string
     */
    public function getDna1() {
        return $this->dna1;
    }

    /**
     * @return string
     */
    public function getDna2() {
        return $this->dna2;
    }

    /**
     * @return int
     */
    public function getScore() {
        return $this->score;
    }

    /**
     * Jednoduchy vypis vysledku zarovnani (pro vypis do konzole nebo do html tagu <pre>).
     * @return string
     */
    public function getPlain() {
        $toReturn = $this->dna1Align . "<br/>";
        for ($i = 0; $i < strlen($this->dna1Align); $i++) {
            if ($this->dna1Align[$i] == $this->dna2Align[$i] && $this->dna1Align[$i] != '-') {
                $toReturn .= "|";
            }
            else {
                $toReturn .= " ";
            }
        }
        $toReturn .= "<br/>" . $this->dna2Align;
        $toReturn = str_replace(" ","&nbsp;",$toReturn); // nahrazeni mezer nedelivymi mezerami - umoznuje pouzit nowrap
        return $toReturn;
    }

}
