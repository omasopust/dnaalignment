<?php

/**
 * User: Petr, 10:57 - 19.10.13
 * Implementace algoritmu Smith-Waterman pro hledani lokalniho zarovani dvou retezcu DNA.
 */
class LocalAlign {

    /** @var ScoreMatrix skorovaci matice */
    protected $scoreMatrix;

    /** @var int sankce za mezeru */
    protected $gapPenalty = -4;

    /** @var Matrix matice hodnot pro vypocet skore dynamickym programovanim */
    protected $matrix;

    /** @const int|char - konstanty urcujici obsah matice smeru */
    const FROM_UP = '|';           //1
    const FROM_LEFT = '-';         //2
    const FROM_UP_LEFT = '\\';     //3
    const NOWAY = 0;

    /** @var Matrix - matice smeru - kazda bunka obsahuje inforamaci o tom z jakeho smeru sme do ni pri vypoctu dosli */
    protected $directionMatrix;

    /** @var string - zadane retezce DNA na porovnani */
    protected $dna1;
    protected $dna2;

    /**
     * @param int $matchScore
     * @param int $mismatchScore
     * @param int $gapPenalty
     */
    public function __construct($matchScore = 5, $mismatchScore = -3, $gapPenalty = 4) {
        $this->scoreMatrix = new ScoreMatrix($matchScore, $mismatchScore);
    }

    /**
     * Metoda pro porovnani dvou retezcu DNA a ziskani jejich lokalniho zarovnani. Nejprve je proveden vypocet cele
     * matice hodnot a pak je zpetne podle matice smeru zjisteno zarovani.
     * @param $dna1
     * @param $dna2
     * @return LocalAlignResult zarovnani retezcu
     */
    public function compare($dna1, $dna2) {
        $this->dna1 = strtoupper($dna1);
        $this->dna2 = strtoupper($dna2);
        $this->validateString($this->dna1);
        $this->validateString($this->dna2);

        //echo "Comparing strings:\n";
        //echo "DNA1 (".strlen($this->dna1)."): $this->dna1\n";
        //echo "DNA2 (".strlen($this->dna2)."): $this->dna2\n";
        //echo $this->scoreMatrix;

        $rows = strlen($dna2)+1;
        $cols = strlen($dna1)+1;
        $this->matrix = new Matrix($rows, $cols);
        $this->directionMatrix = new Matrix($rows, $cols, self::NOWAY);

        $this->computeMatrix($dna1, $dna2);
        //echo "Work matrix:\n" . $this->matrix;
        //echo "Direction matrix:\n" . $this->directionMatrix;
        //echo "Maximum Score: " . $this->matrix->getMax() . " - [" . $this->matrix->getMaxPosition()->row() . "," . $this->matrix->getMaxPosition()->col() ."]\n";

        if ($this->matrix->getMax() <= 0) { // matice si nejsou vubec podobne -> vracime skore: 0
            return new LocalAlignResult($this->dna1, $this->dna2, 0, "", "");
        }

        return $this->getAlign($this->matrix->getMaxPosition());
    }

    /**
     * Validace zadavanych retezcu DNA (nesmi obsahovat bile znaky)
     * @param $dna
     * @return bool
     * @throws InvalidArgumentException pokud se nejedna o platny retezec DNA
     */
    public function validateString($dna) {
        if (preg_match("/^[ACTG]+$/", $dna)) {
            return true;
        }
        throw new \InvalidArgumentException("String '$dna' is not valid DNA string. DNA contains only 'A', 'C', 'T', 'G'.");
    }

    /**
     * Vypoceteni obsahu matice hodnot a matice smeru.
     */
    protected function computeMatrix() {
        for ($i = 1; $i < $this->matrix->rows(); $i++) {
            for ($j = 1; $j < $this->matrix->cols(); $j++) {
                $max = - (PHP_INT_MAX - 1);
                $maxDirection = self::NOWAY;
                $values = array(
                    self::FROM_UP        => $this->matrix->at($i-1,$j) + $this->gapPenalty,   // from up
                    self::FROM_LEFT      => $this->matrix->at($i,$j-1) + $this->gapPenalty,   // from left
                    self::FROM_UP_LEFT   => $this->matrix->at($i-1,$j-1) + $this->getScore($i, $j),   // from up-left
                    self::NOWAY          => 0  // no way
                );

                foreach (array_keys($values) as $key) {
                    if ($values[$key] > $max) {
                        $max = $values[$key];
                        $maxDirection = $key;
                    }
                }

                $this->matrix->set($i,$j,$max);
                $this->directionMatrix->set($i,$j,$maxDirection);
            }
        }
    }

    /**
     * Zjisteni skore ze skorovaci matice podle zadane pozice v matici hodnot.
     * @param int $i
     * @param int $j
     * @return int
     */
    protected function getScore($i, $j) {
        return $this->scoreMatrix->getScore($this->dna2[$i-1], $this->dna1[$j-1]);
    }

    /**
     * Zpetne odvozeni lokalniho zarovnani retezcu podle vypoctene matice hodnot a matice smeru.
     * Pro kazdy retezec DNA je vytvoren dalsi retezec reprezentujici jeho zarovnani obsahujeci A,C,T,G, mezery a pomlcky.
     * Retezce obsahujici zarovnani, jsou konstruovany od konce a nakonec jsou prevraceny.
     * @param Position $maxPosition - pozice maxima v matici hodnot
     * @return LocalAlignResult - vysledne zarovnani retezcu
     */
    protected function getAlign(Position $maxPosition) {
        $row = $maxPosition->row();
        $col = $maxPosition->col();
        $s1 = $s2 = "";
        // Vypsani znaku za zarovnanou casti retezcu.
        for ($c = $this->matrix->cols()-1; $c > $col; $c--) {   // vypis znaku v poslednim sloupci od posledniho pred pozici maxima
            $s1 .= $this->dna1[$c-1];
            $s2 .= " ";
        }
        for ($r = $this->matrix->rows()-1; $r > $row; $r--) {   // vypis znaku v poslednim radku od posledniho pred pozici maxima
            $s1 .= " ";
            $s2 .= $this->dna2[$r-1];
        }
        $s1 .= "-"; $s2 .= "-"; // oddeleni zarovnavane casti pomlcou
        // Synchronizovane vypsani zarovnane casti retezcu
        while ($this->matrix->at($row,$col) != 0) {
            //echo "[".$position[0].",".$position[1]."] -> ";
            //echo "[".$this->dna2[$row - 1]."-".$this->dna1[$col - 1]."] -> ";
            switch ($this->directionMatrix->at($row, $col)) {
                case self::FROM_LEFT: { // prisel jsem zleva -> j-1
                    $s1 .= $this->dna1[$col-1]; $s2 .= "-";
                    $col--;
                    break;
                }
                case self::FROM_UP: {   // prisel jsem zhora -> i-1
                    $s1 .= "-"; $s2 .= $this->dna2[$row-1];
                    $row--;
                    break;
                }
                case self::FROM_UP_LEFT: {  // prisel jsem zleva zhora -> i-1, j-1
                    $s1 .= $this->dna1[$col-1];
                    $s2 .= $this->dna2[$row-1];
                    $row--;
                    $col--;
                    break;
                }
                case self::NOWAY: { }
            }
        }
        $s1 .= "-"; $s2 .= "-"; // oddeleni zarovnavane casti pomlcou
        // Vypsani znaku pred zarovnanou casti retezcu
        for ($c = $col; $c > 0; $c--) { // vypis znaku v prvnim sloupci od prvni pozice zarovnane casti do 0
            $s1 .= $this->dna1[$c-1];
            $s2 .= " ";
        }
        for ($r = $row; $r > 0; $r--) { // vypis znaku v prvnim radku od prvni pozice zarovnane casti do 0
            $s1 .= " ";
            $s2 .= $this->dna2[$r-1];
        }
        //echo "[".$row.",".$col."]\n" . strrev($s1)."\n".strrev($s2)."\n";
        return new LocalAlignResult($this->dna1, $this->dna2, $this->matrix->getMax(), strrev($s1), strrev($s2));
    }

}
