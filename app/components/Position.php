<?php
/**
 * User: Petr, 14:17 - 2.11.13
 * Trida reprezentujici pozici v matici.
 */
class Position {

    /**
     * @var int
     */
    protected $row;
    protected $col;

    /**
     * @param int $row
     * @param int $col
     */
    public function __construct($row, $col) {
        $this->row = $row;
        $this->col = $col;
    }

    /**
     * @return int
     */
    public function col() {
        return $this->col;
    }

    /**
     * @return int
     */
    public function row() {
        return $this->row;
    }

}
