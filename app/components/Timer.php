<?php

use Nette\Diagnostics\Debugger;
/**
 * User: Petr, 16:19 - 28.11.13
 * Stopky umoznujici stopovat vice casovych useku najednou. Postaveno na Nette::Debugger stopkach.
 */
class Timer {

    const MINUTES = 0;
    const SECONDS = 1;
    const MILISECONDS= 2;
    const MICROSECOND= 3;

    const AS_ARRAY = 4;
    const AS_INTEGER = 5;
    const AS_FLOAT = 6;

    /**
     * @var array pole stopovanych useku
     */
    protected static $times = array();

    /**
     * @param string $name nazev stopovaneho useku
     */
    public static function start($name) {
        Debugger::timer($name);
    }

    /**
     * @param string $name nazev stopovaneho useku
     */
    public static function stop($name) {
        self::$times[$name] = Debugger::timer($name);
    }

    /**
     * Vraci namereny cas jako hodnotu v mikrosekundach nebo jako pole integeru clenene podle casovych jednotek.
     * @param string $name nazev stopovaneho useku
     * @param int $format format vystupu
     * @return array|float|int
     */
    public static function getTime($name, $format = self::AS_FLOAT) {
        self::validateTimer($name);
        if ($format == self::AS_FLOAT) {
            return self::$times[$name]; // v mikrosekundach
        }
        $microTime = ceil(self::$times[$name] * 1000 * 1000);  // v mikrosekundach
        if ($format == self::AS_INTEGER) {
            return $microTime;
        }
        else {  // metoda vraci pole
            $microSeconds = $microTime % 1000;
            $microTime /= 1000;
            $miliSeconds = $microTime % 1000;
            $microTime /= 1000;
            $seconds = $microTime % 1000;
            $microTime /= 1000;
            $minutes = $microTime % 60;
            return array(
                self::MINUTES => $minutes,          // 0
                self::SECONDS => $seconds,          // 1
                self::MILISECONDS => $miliSeconds,  // 2
                self::MICROSECOND => $microSeconds, // 3
                "m" => $minutes,
                "s" => $seconds,
                "ms" => $miliSeconds,
                "mms" => $microSeconds,
            );
        }
    }

    /**
     * Formatovany vypis namereneho casu.
     * Formatovaci znaky:
        * %m - minuty
        * %s - sekundy
        * %ms - milisekundy
        * %mms - mikrosekundy
     * @param string $name nazev stopovaneho useku
     * @param string $formatString formatovaci retezec (jako u printf)
     * @return string naformatovany retezec
     */
    public static function printTime($name, $formatString = "%m min, %s s, %ms.%mms ms") {
        $time = self::getTime($name, self::AS_ARRAY);
        $formatString = preg_replace("/\%mms/", $time["mms"], $formatString);
        $formatString = preg_replace("/\%ms/", $time["ms"], $formatString);
        $formatString = preg_replace("/\%s/", $time["s"], $formatString);
        $formatString = preg_replace("/\%m/", $time["m"], $formatString);
        return $formatString;
    }

    /**
     * Overeni zda zadane stopky jiz ukoncily mereni a je ulozen namereny cas.
     * @param string $name nazev stopovaneho useku
     * @throws Nette\InvalidArgumentException
     */
    protected static function validateTimer($name) {
        if (!isset(self::$times[$name])) {
            throw new \Nette\InvalidArgumentException("Timer '$name' haven't started or stopped.");
        }
    }

}
