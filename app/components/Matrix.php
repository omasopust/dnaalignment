<?php
/**
 * User: Petr, 11:47 - 2.11.13
 * Reprezentace obecne matice.
 */
class Matrix {

    /** @var array */
    protected $matrix;

    /** @var int rozmery matice */
    protected $rows;
    protected $cols;

    /** @var Position|null - souradnice, na kterych se nachazi v matici nejvetsi hodnota */
    protected $maxPosition;

    /**
     * @param int $rows
     * @param int $cols
     * @param mixed $initValue
     */
    public function __construct($rows, $cols, $initValue = 0) {
        $this->rows = $rows;
        $this->cols = $cols;

        $this->matrix = array();
        for ($i=0; $i < $rows; $i++) {
            $this->matrix[$i] = array_fill_keys(range(0, $cols-1), $initValue);
        }

        $this->maxPosition = new Position(0,0);
    }

    /**
     * @return int
     */
    public function rows() {
        return $this->rows;
    }

    /**
     * @return int
     */
    public function cols() {
        return $this->cols;
    }

    /**
     * Vraci prvek v matici na pozici [row, col].
     * @param int $row
     * @param int $col
     * @return mixed
     * @throws InvalidArgumentException - vyhazuje vyjimku v pripade neplatneho indexu.
     */
    public function at($row, $col) {
        if (!isset($this->matrix[$row][$col])) {
            throw new \InvalidArgumentException(
                "Invalid indexes '$row' and '$col' to matrix. Valid indexes are [0-" . ($this->rows-1) . "][0-" . ($this->cols-1) . "]."
            );
        }
        return $this->matrix[$row][$col];
    }

    /**
     * Nastavuje v prvku matici na pozici [row, col] zadanou hodnotu val. Pokud je nova hodnota vetsi nez stavajici
     * maximalni hodnota matice, je nastavena jako nova maximalni hodnota v matici.
     * @param int $row
     * @param int $col
     * @param mixed $val
     * @throws InvalidArgumentException - vyhazuje vyjimku v pripade neplatneho indexu.
     */
    public function set($row, $col, $val) {
        if (!isset($this->matrix[$row][$col])) {
            throw new \InvalidArgumentException(
                "Invalid indexes '$row' and '$col' to matrix. Valid indexes are [0-" . ($this->rows-1) . "][0-" . ($this->cols-1) . "]."
            );
        }
        $this->matrix[$row][$col] = $val;
        // nastaveni maxima matice
        if ($this->matrix[$row][$col] > $this->matrix[$this->maxPosition->row()][$this->maxPosition->col()]) {
            $this->maxPosition = new Position($row, $col);
        }
    }

    /**
     * Vraci nejvetsi hodnotu v matici. Pokud neni maximalni hodnota v matici jeste nastavena nebo je parametr
     * $forceSearch == TRUE, pak je maximalni hodnota vypoctena prohledanim cele matice.
     * @param bool $forceSearch - pokud je TRUE pak se prohledavani matice provede vzdy.
     * @return mixed
     */
    public function getMax($forceSearch = false) {
        if ($this->maxPosition === null || $forceSearch) {  // pokud jeste nebylo maximum nalezeno, spust hledani maxima
            $this->findMaximum();
        }
        return $this->matrix[$this->maxPosition->row()][$this->maxPosition->col()];
    }

    /**
     * Vraci pozici maxima v matici. Pokud neni maximalni hodnota v matici jeste nastavena nebo je parametr
     * $forceSearch == TRUE, pak je maximalni hodnota vypoctena prohledanim cele matice.
     * @param bool $forceSearch - pokud je TRUE pak se prohledavani matice provede vzdy.
     * @return Position
     */
    public function getMaxPosition($forceSearch = false) {
        if ($this->maxPosition === null || $forceSearch) {
            $this->findMaximum();
        }
        return $this->maxPosition;
    }

    /**
     * Jednoduchy vypis matice.
     * @return string
     */
    public  function __toString() {
        $toReturn = "";
        for ($i = 0; $i < $this->rows; $i++) {
            for ($j = 0; $j < $this->cols; $j++) {
                $toReturn .= $this->getSpaces($this->matrix[$i][$j]) . $this->matrix[$i][$j];
            }
            $toReturn .= "\n";
        }
        return $toReturn;
    }

// ----------------------------------------------------------------------------

    /**
     * Metoda provde nalezeni maxima v matici a ulozeni jeho pozice v matici. Metoda prohledava pouze posledni
     * radek a posledni sloupec matice.
     * @return Position
     */
    protected function findMaximum() {
        $max = $row = $col = 0;
        for ($i = 1; $i < $this->rows; $i++) {
            for ($j = 1; $j < $this->cols; $j++) {
                if ($this->matrix[$i][$j] > $max) {
                    $max = $this->matrix[$i][$j];
                    $row = $i;
                    $col = $j;
                }
            }
        }
        $this->maxPosition = new Position($row, $col);
        return $this->maxPosition;
    }

    /**
     * Pomocna metoda pro zarovnani hodnot ve vypisu matice, podle velikost hodnoty vypise patricny pocet mezer
     * pred touto honotou.
     * @param string $item
     * @return string mezery
     */
    protected function getSpaces($item) {
        switch (strlen($item)) {
            case 1: return "  ";
            case 2: return " ";
            default: return "";
        }
    }

}
