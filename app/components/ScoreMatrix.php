<?php
/**
 * User: Petr, 10:48 - 2.11.13
 * Reprezentace skorovaci matice.
 */
class ScoreMatrix {

    /** @var array */
    protected $matrix;

    /**
     * Vytvori matici 4x4 s hodnotami matchScore na diagonale a hodnotami mismatchScore vsude jinde.
     * @param int $m matchScore = hodnota v matici za shodu
     * @param int $f mismatchScore = hondnota v matici za neshodu
     */
    public function __construct($m, $f) {
        $this->matrix = array(
            //                   A          C          T          G
            "A" => array("A" => $m, "C" =>$f, "T" =>$f, "G" =>$f),  // A
            "C" => array("A" =>$f, "C" => $m, "T" =>$f, "G" =>$f),  // C
            "T" => array("A" =>$f, "C" =>$f, "T" => $m, "G" =>$f),  // T
            "G" => array("A" =>$f, "C" =>$f, "T" =>$f, "G" => $m)   // G
        );
    }

    /**
     * Dotaz na hodnotu skore pro dva zadane znaky. Povolene hodnoty indexu jsou A,C,T,G.
     * @param string $row - radek skorovaci matice
     * @param string $col - sloupec skorovaci matice
     * @return int hodota skore
     * @throws InvalidArgumentException v pripade ze jsou zadane neplatne indexy (znaky DNA retezce)
     */
    public function getScore($row, $col) {
        if (isset($this->matrix[$row][$col])) {
            return $this->matrix[$row][$col];
        }
        else {
            throw new \InvalidArgumentException("Invalid indexes '$row' and '$col' to score matrix. Valid indexes are 'A', 'C', 'T' ,'G'.");
        }
    }

    /**
     * Jednoduchy vypis matice.
     * @return string
     */
    public function __toString() {
        $toReturn = "Score matrix:\n";
        foreach ($this->matrix as $row) {
            foreach ($row as $cell) {
                $toReturn .= $this->getSpaces($cell) . $cell;
            }
            $toReturn .= "\n";
        }
        return $toReturn;
    }

// ----------------------------------------------------------------------------

    /**
     * Pomocna metoda pro zarovnani hodnot ve vypisu matice, podle velikost hodnoty vypise patricny pocet mezer
     * pred touto honotou.
     * @param string $item
     * @return string mezery
     */
    protected function getSpaces($item) {
        switch (strlen($item)) {
            case 1: return "  ";
            case 2: return " ";
            default: return "";
        }
    }

}
