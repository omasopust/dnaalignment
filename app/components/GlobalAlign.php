<?php

/**
 * Algoritmus 
 *
 * @author Ondřej Masopust
 */


class GlobalAlign {
    
    // Konstanty směru
    const NO_WAY = 0;
    const FROM_UP = 1;
    const FROM_UP_LEFT = 2;
    const FROM_LEFT = 3;
    
    // Skorovaci hodnoty
    private $matchScore;
    private $mismatchScore;
    private $gapScore;
    
    private $dna1;
    private $dna2;
    
    private $matrix;
    private $directionMatrix;
    
    public function __construct($matchScore = 5, $mismatchScore = -3, $gapScore = -4) {
        $this->matchScore = $matchScore;
        $this->mismatchScore = $mismatchScore;
        $this->gapScore = $gapScore;
    }
    
    public function compare($dna1,$dna2) {
        $this->dna1 = strtoupper($dna1);
        $this->dna2 = strtoupper($dna2);
        
        //TODO validace
        
        // Spočítam počty řádků a sloupců v matrixu
        $cols = strlen($dna1)+1; // DNA horizontální
        $rows = strlen($dna2)+1; // DNA vertikální
        
        
        $this->matrix = new Matrix($rows, $cols);
        $this->directionMatrix = new Matrix($rows, $cols, self::NO_WAY);
        
        $this->computeMatrix($rows, $cols);
        
        if ($this->matrix->getMax() <= 0) { // matice si nejsou vubec podobne -> vracime skore: 0
            return new LocalAlignResult($this->dna1, $this->dna2, 0, "", "");
        }

        return $this->getAlign($this->matrix->getMaxPosition());
    }
    
    protected function computeMatrix($rows, $cols) {
        // Nastavení hodnot na prvním řádku (row = 0)
        for($col = 0; $col < $cols; $col++) {
            $this->matrix->set(0, $col, $col * $this->gapScore);
            // nastaneni direction, vynechavam pocatek
            if($col > 0) {
                $this->directionMatrix->set(0, $col, self::FROM_LEFT);
            }
        }
        
        // Nastavení hodnot v prvním sloupci (col = 0)
        for($row = 0; $row < $rows; $row++) {
            $this->matrix->set($row, 0, $row * $this->gapScore);
            // nastaneni direction, vynechavam pocatek
            if($row > 0) {
                $this->directionMatrix->set($row, 0, self::FROM_UP);
            }
        }
         
        // Vyplnuju hodnoty celeho matrixu
        for($row = 1; $row < $rows; $row++) {           // Pro každé políčko
            for($col = 1; $col < $cols; $col++) {       // 
                
                // Porovname odpovidajici prvky z obou DNA
                if(substr($this->dna1, $col - 1, 1) == substr($this->dna2, $row - 1, 1)) {
                    $compareScore = $this->matchScore;
                } else {
                    $compareScore = $this->mismatchScore;
                }
                
                $pathScores = array(
                    self::FROM_LEFT => $this->matrix->at($row, $col - 1) + $this->gapScore,
                    self::FROM_UP_LEFT => $this->matrix->at($row - 1, $col - 1) + $compareScore,
                    self::FROM_UP => $this->matrix->at($row - 1, $col) + $this->gapScore
                );
                
                // Ohodnoceni pole nejvetsi hodnotou predchudcu
                $maxScore = $max = -(PHP_INT_MAX - 1);     // Nejnizsi mozna hodnota
                foreach($pathScores as $path => $score) {
                    if($score > $maxScore) {
                        $maxScore = $score;
                        $direction = $path;
                    }
                }
                
                // Přiřazení maxima a jeho cesty do matrixu
                $this->matrix->set($row, $col, $maxScore);
                $this->directionMatrix->set($row, $col, $direction);
            }
        }
    }
    
    /**
     * Zpetne odvozeni lokalniho zarovnani retezcu podle vypoctene matice hodnot a matice smeru.
     * Pro kazdy retezec DNA je vytvoren dalsi retezec reprezentujici jeho zarovnani obsahujeci A,C,T,G, mezery a pomlcky.
     * Retezce obsahujici zarovnani, jsou konstruovany od konce a nakonec jsou prevraceny.
     * @param Position $maxPosition - pozice maxima v matici hodnot
     * @return LocalAlignResult - vysledne zarovnani retezcu
     */
    protected function getAlign(Position $maxPosition) {
        $row = $maxPosition->row();
        $col = $maxPosition->col();
        $s1 = $s2 = "";
        // Vypsani znaku za zarovnanou casti retezcu.
        for ($c = $this->matrix->cols()-1; $c > $col; $c--) {   // vypis znaku v poslednim sloupci od posledniho pred pozici maxima
            $s1 .= $this->dna1[$c-1];
            $s2 .= " ";
        }
        for ($r = $this->matrix->rows()-1; $r > $row; $r--) {   // vypis znaku v poslednim radku od posledniho pred pozici maxima
            $s1 .= " ";
            $s2 .= $this->dna2[$r-1];
        }
        $s1 .= "-"; $s2 .= "-"; // oddeleni zarovnavane casti pomlcou
        // Synchronizovane vypsani zarovnane casti retezcu
        while ($this->matrix->at($row,$col) != 0) {
            //echo "[".$position[0].",".$position[1]."] -> ";
            //echo "[".$this->dna2[$row - 1]."-".$this->dna1[$col - 1]."] -> ";
            switch ($this->directionMatrix->at($row, $col)) {
                case self::FROM_LEFT: { // prisel jsem zleva -> j-1
                    $s1 .= $this->dna1[$col-1]; $s2 .= "-";
                    $col--;
                    break;
                }
                case self::FROM_UP: {   // prisel jsem zhora -> i-1
                    $s1 .= "-"; $s2 .= $this->dna2[$row-1];
                    $row--;
                    break;
                }
                case self::FROM_UP_LEFT: {  // prisel jsem zleva zhora -> i-1, j-1
                    $s1 .= $this->dna1[$col-1];
                    $s2 .= $this->dna2[$row-1];
                    $row--;
                    $col--;
                    break;
                }
                case self::NOWAY: { }
            }
        }
        $s1 .= "-"; $s2 .= "-"; // oddeleni zarovnavane casti pomlcou
        // Vypsani znaku pred zarovnanou casti retezcu
        for ($c = $col; $c > 0; $c--) { // vypis znaku v prvnim sloupci od prvni pozice zarovnane casti do 0
            $s1 .= $this->dna1[$c-1];
            $s2 .= " ";
        }
        for ($r = $row; $r > 0; $r--) { // vypis znaku v prvnim radku od prvni pozice zarovnane casti do 0
            $s1 .= " ";
            $s2 .= $this->dna2[$r-1];
        }
        //echo "[".$row.",".$col."]\n" . strrev($s1)."\n".strrev($s2)."\n";
        return new GlobalAlignResult($this->dna1, $this->dna2, $this->matrix->getMax(), strrev($s1), strrev($s2));
    }
    
    
}
